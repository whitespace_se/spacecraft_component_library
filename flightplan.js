let plan = require('flightplan');
let spacecraftConfig = require('./config');

let config = {
  keepReleases: 5
};

// Change target & update host/hostname
plan.target('dev', {
  host: 'stil.guide',
  username: 'whitespace',
  agent: process.env.SSH_AUTH_SOCK
}, {
  root: '/srv/www',
  targets: ['.stil.guide'],
  user: 'web'
});

let shared = {};

/*
 * init
 *
 * Initialize the server by setting up a suitable project structure.
 */
plan.remote('init', function(remote) {
  let root = plan.runtime.options.root;
  let targets = plan.runtime.options.targets;
  let user = plan.runtime.options.user;

  targets.forEach(function(target) {
    let dir = root + '/' + target;

    remote.sudo('mkdir -p ' + dir + '/releases', {
      user: user
    });
    remote.sudo('mkdir -p ' + dir + '/shared', {
      user: user
    });
  });
});

/*
 * deploy
 *
 * Deploy a release to the server. Will deploy to a specific sub folder
 * and symlink it as the current release.
 */
let time = new Date().toISOString();
let tmpDir = '/tmp/app' + time;

plan.local('deploy', function(local) {
  function planLocal() {
    local.log('Generate the styleguide');
    let files = local.find(spacecraftConfig.root.production, {
      silent: true
    });
    local.transfer(files, tmpDir);
  }

  function createProductionFolder(callback) {
    local.exec('gulp styleguide');
    callback();
  }

  createProductionFolder(planLocal);
});

plan.remote('deploy', function(remote) {
  let root = plan.runtime.options.root;
  let targets = plan.runtime.options.targets;
  let user = plan.runtime.options.user;

  targets.forEach(function(target) {
    let dir = root + '/' + target;
    let release = dir + '/releases/' + time;

    remote.log('Deploying subsite ' + target + ' to ' + release);
    remote.sudo('cp -R ' + tmpDir + '/' + spacecraftConfig.root.production + ' ' + release, {
      user: user
    }, {
      user: user
    });

    Object.keys(shared).forEach(function(key) {
      let s = dir + '/shared/' + key;
      let name = shared[key];
      if (name === true) {
        name = key;
      }
      let t = release + '/' + name;
      remote.sudo('ln -s ' + s + ' ' + t, {
        user: user
      });
    });

    remote.sudo('rm ' + dir + '/current', {
      failsafe: true,
      user: user
    });
    remote.sudo('ln -s ' + dir + '/releases/' + time + ' ' + dir + '/current', {
      user: user
    });

    remote.log('Checking for stale releases');
    let releases = getReleases(remote, dir);
    if (releases.length > config.keepReleases) {
      let removeCount = releases.length - config.keepReleases;
      remote.log('Removing ' + removeCount + ' stale release(s)');
      releases = releases.slice(0, removeCount);
      releases = releases.map(function(item) {
        return dir + '/releases/' + item;
      });
      remote.sudo('rm -rf ' + releases.join(' '));
    }
  });

  remote.rm('-R ' + tmpDir);
});

// Remove old releases
// Usage: fly rollback:staging
plan.remote('rollback', function(remote) {
  let root = plan.runtime.options.root;
  let targets = plan.runtime.options.targets;
  let user = plan.runtime.options.user;

  targets.forEach(function(target) {
    let dir = root + '/' + target;
    remote.log('Rolling back release');
    let releases = getReleases(remote, dir);
    if (releases.length > 1) {
      let oldCurrent = releases.pop();
      let newCurrent = releases.pop();
      remote.log('Linking current to ' + newCurrent);
      remote.sudo('ln -nfs ' + dir + '/releases/' + newCurrent + ' ' + dir + '/current');
      remote.log('Removing ' + oldCurrent);
      remote.sudo('rm -rf ' + dir + '/releases/' + oldCurrent);
    }
  });

});

function getReleases(remote, dir) {
  let releases = remote.sudo('ls ' + dir + '/releases', {
    silent: true
  });

  if (releases.code === 0) {
    releases = releases.stdout.trim().split('\n');
    return releases;
  }
  return [];
}

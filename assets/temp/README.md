# Temporary assets
This directory is for temporary files.

### Tasks and Files
```
gulpfile.js/tasks/temp
```

Files are copied from this folder to the `root.dest` folder specified in `config.json`.

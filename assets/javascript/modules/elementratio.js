export default class ElementRatio {
  constructor(element){

    let that = this
    this.element = element
    this.element.style.display = 'block'

    this.x = element.getAttribute('data-x')
    this.y = element.getAttribute('data-y')

    // Trigger on load and window resize
    this.setRatio()
    window.addEventListener('resize', function() {
        that.setRatio()
    }, true)
  }

  setRatio() {
    this.element.style.height = this.element.offsetWidth/this.x*this.y + 'px'
  }
}

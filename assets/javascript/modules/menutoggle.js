export default class MenuToggle {
  constructor(element) {

    let that = this

    let toggleClassName = element.getAttribute('data-toggle-class')
    let toggleBtnClassName = element.getAttribute('data-toggle-btn')

    this.toggleClassName = toggleClassName
    this.toggleBtn = element.getElementsByClassName(toggleBtnClassName)

    if (!this.toggleClassName || !this.toggleBtn) { return }

    for (var i = 0; i < this.toggleBtn.length; i++) {
      this.toggleBtn[i].addEventListener('click', function() {
        that.toggle(this)
      })
    }
  }

  toggle(btn) {

    /* Remove fixed height from animation to make
    menu expand in height depending on toggle */
    let menuWrapper = document.getElementsByClassName('m-main-nav__wrapper')[0]
    menuWrapper.style.height = 'auto'

    let target = btn.nextElementSibling
    let link = ''

    if( !target.classList.contains(this.toggleClassName) ) {

      if(link = this.previousElementSibling || link == 'A') {
        link.classList.add(toggleClassName)
      }
      target.classList.add(this.toggleClassName)
      btn.classList.add(this.toggleClassName)
      btn.removeAttribute('aria-hidden')
    } else {

      if(this.link = this.previousElementSibling || this.link == 'A') {
        this.link.classList.remove(this.toggleClassName)
      }
      target.classList.remove(this.toggleClassName)
      btn.classList.remove(this.toggleClassName)
      btn.setAttribute('aria-hidden', true)
    }
  }
}

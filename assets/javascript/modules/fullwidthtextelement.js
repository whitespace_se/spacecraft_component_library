export default class FullwidthTextElement {
  constructor(element) {

    let that = this
    this.element = element
    let targetClassName = element.getAttribute('data-target-class')
    this.text = element.getElementsByClassName(targetClassName)[0]

    // Trigger on load and window resize
    this.setTextSize()
    window.addEventListener('resize', function() {
      that.setTextSize()
    })
  }

  setTextSize() {

    // Element width, content width and fontsize
    let x = this.text.offsetWidth
    let y = this.element.offsetWidth
    let z = parseFloat(window.getComputedStyle(this.text, null).getPropertyValue('font-size'))

    // If content is smaller than parentelement
    if(x < y) {
      for (let i = x; i < y; z++) {
        this.text.style.fontSize = z + 'px'
        this.text.style.lineHeight = z + 'px'
        i = this.text.offsetWidth
      }

    // If content is bigger than parentelement
    } else {
      for (let i = x; i > y; z--) {
        this.text.style.fontSize = z + 'px'
        this.text.style.lineHeight = z + 'px'
        i = this.text.offsetWidth
      }
    }
  }
}

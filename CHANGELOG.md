# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.3.5] - 2017-11-07

### Changed
- Updated `spacecraft` to `0.3.3`

## [0.3.4] - 2017-11-06

### Updated
- `flightplan.js` - bug fix.

## [0.3.3] - 2017-11-06

### Updated
- `flightplan.js` - uses `config.json` for paths, changed default target url, cleaning up js-code.

### Changed
- Updated `spacecraft` to `0.3.3`

## [0.3.2] - 2017-10-15

### Added
- `_master.twig` & `_preview.twig` in the `components` folder.
- Added `temp` folder in the `assets` folder & a `README.md` file

### Updated
- Updated `config.json` - added code that re-arrange `panels` default order is:`["notes","html", "view", "context", "resources", "info"]
- Updated `spacecraft` to `0.3.2`

## [0.3.1] - 2017-10-11

### Added
- CHANGELOG.md (this file)
- Added `spacecraft` command

### Changed
- Updated `spacecraft` to `0.3.1`
- Updated `config.json` to follow the new structure of `spacecraft` `0.3.1`

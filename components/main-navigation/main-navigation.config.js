
'use strict';

let navObjPrototype =
{
  title: 'lorem ipsum chips',
  url: '#',
  current: '',
  items: [
    {
      title: 'lorem ipsum chips',
      url: '#',
      current: '',
      items: []
    },
    {
      title: 'lorem ipsum chips',
      url: '#',
      current: '',
      items: [
        {
          title: 'lorem ipsum chips',
          url: '#',
          current: '',
          items: [
            {
              title: 'lorem ipsum chips',
              url: '#',
              current: '',
              items: []
            }
          ]
        }
      ]
    }
  ]
}

let itemsData = [];
let navItem = [];

for (var i = 0; i < 2; i++) {
  navItem.push(navObjPrototype);
}

for (var i = 0; i < 3; i++) {
  itemsData.push(navItem);
}

module.exports = {
  status: 'wip',
  context: {
    items: itemsData
  }
};
